import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

// 2n + 1 left child
//2n + 2 right child
public class heap {
	private ArrayList<Integer> A;	
	public heap () {
		A =new ArrayList<Integer>();

	}
	public void add (int value) {
		A.add(value);
		int n = A.size() - 1;
		while ( (A.get(n) < A.get((n-1)/2) && n%2 == 1)|| (A.get(n) < A.get((n-2)/2) && n%2 == 0 ))  {
			int temp = 0;
			if ( n%2 == 1) {
				temp = A.get((n-1)/2);
				A.set(((n-1)/2), A.get(n));
				A.set(n, temp);
				n = (n-1)/2;
			}
			else if (n%2 == 0) { 
				temp = A.get((n-2)/2);
				A.set(((n-2)/2), A.get(n));
				A.set(n,  temp);
				n = (n-2)/2;
			}
		}

	}
	public int getMin () {
		return A.get(0);        

	}
	public int removeHead () {
		int head = A.get(0);
		A.set(0, A.get(A.size() - 1));
		A.remove(A.size() - 1);
		int n = 0;
		// 2n + 1 left child
		//2n + 2 right child
		while ( (A.get(n) > A.get(2 *n+1)  && (n*2 + 1 < A.size()))|| (A.get(n) > A.get(2*n+ 2) && (n*2 + 2 < A.size())))   {
			if (2*n + 2 < A.size()|| A.get(n*2 + 2) > A.get(2*n+1)) {
				int temp = A.get(n);
				A.set(A.get(n),A.get(2*n + 1));
				A.set(A.get(2*n + 1), temp);
				n = n*2 + 1; 
			}
			else if (2*n + 1 < A.size()&& A.get(n*2 + 1) > A.get(2*n+2)) {
				int temp = A.get(n);
				A.set(A.get(n),A.get(2*n + 2));
				A.set(A.get(2*n + 2), temp);
				n = n*2 + 2; 
			}
		}
		return head;

	}
	public void remove (int i) {
		A.set(i, Integer.MIN_VALUE);
		int n = i;

		while(n > 0) {
			if ( n%2 == 0) {
				int temp = (n-2)/2;
				A.set(temp, A.get(n));
				A.set(i, temp);
				n = n/2 - 2;
			}
			else if ( n%2 == 1) {
				int temp = (n-1)/2;
				A.set(temp, A.get(n));
				A.set(i, temp);
				n = n/2 - 1;
			}

		}
		removeHead();


	}
	public void changeValue (int n, int value ) {
		int a = n%2;
		A.set(n, value);

		if ((A.get(n*2 + 2) < A.get(n) && n*2 + 2 < A.size()) || (A.get(n*2+1) < A.get(n) && n*2 + 1 < A.size())) {
			while ((A.get(n*2 + 2) < A.get(n) && n*2 + 1 < A.size() - 1)||( A.get(n*2+1) < A.get(n) && n*2 + 1 < A.size())) {
				if ( n*2+2 < A.size() && A.get(n*2 + 2) < A.get(n*2 + 1) ) {
					int temp = A.get(n*2 + 2);
					A.set(n*2 + 2, A.get(n));
					A.set(n, temp);
					n = n*2 + 2;
				}
				else {
					int temp = A.get(n*2 + 1);
					A.set(n*2 + 1, A.get(n));
					A.set(n, temp);
					n = n*2 + 1;

				}
			}
		}
		else if (( A.get((n-2)/2) > A.get(n) && a==0) ||  (A.get((n-1)/2) > A.get(n) && a==1 )  ) {
			while ( A.get((n-2)/2) > A.get(n) || A.get((n-1)/2) > A.get(n) ){
				if ( a== 0) {
					int temp = A.get((n-2)/2);
					A.set((n-2)/2, A.get(n));
					A.set(n, temp);
					n = (n-2)/2;
					a = n%2;
				}
				else if ( a==1) { 
					int temp = A.get((n-1)/2);
					A.set((n-1)/2, A.get(n));
					A.set(n, temp);
					n = (n-1)/2;
					a = n%2;

				}
			}

		}

	}
	public static int[]heapSort (int[] A) {
		//ArrayList<Integer> B = new ArrayList<Integer>();
		int n = 0;

		for ( int i = A.length - 1; i >= 0 ; i--) {
			n = i;
			int a = n%2;
			while (n != 0 && ((A[n] < A[(n-2) / 2] && a== 0) || (A[n] < A[(n-1) / 2] && a==1))) {
				if ( a== 0) {
					int temp = A[(n-2)/2];
					A[(n-2)/2] = A[n];
					A[n] = temp;
					n = (n-2)/2;
					a = n%2;
					i = A.length;

				}
				else if ( a== 1) {
					int temp = A[(n-1)/2];
					A[(n-1)/2] = A[n];
					A[n] = temp;
					n = (n-1)/2;
					a = n%2;
					i = A.length ;

				}

			}
		}
		
		
		for ( int i = 0 ; i < A.length ; i++ ) {
			
			int temp = A[A.length - i - 1];
			A[A.length - i - 1] = A[0];
			A[0] = temp;
			n = 0;
			
			while (( n*2 + 2 < A.length - i -1 && A[n*2 + 2] < A[n] )||( n*2 + 1 < A.length - i - 1 &&  A[n*2+1] < A[n])) {
				if ( n*2+2 < A.length - i - 1 && A[n*2 + 2] < A[n*2 + 1] ) {
					int temps = A[n*2 + 2];
					A[n*2 + 2] = A[n];
					A[n] = temps;
					n = n*2 + 2;
				}
				else {
					int temps = A[n*2 + 1];
					A[n*2+1] = A[n];
					A[n] = temps;
					n = n*2 + 1;


				}
			}
		}
		
		for (int i = 0 ; i < A.length/2 ; i ++) {
			int temp = A[A.length - i - 1];
			A[A.length - i -1] = A[i];
			A[i]= temp;
		}
		return A;
	}
	public static void main(String[] args) {
		int[] A = {7, 3, 8, 5, 4, 32 , 365, 34 , 35, 9};
		System.out.println(Arrays.toString(heapSort(A)));
	}
}