

public class BinaryNode {
	public int data;
	public BinaryNode left;
	public BinaryNode right;
	
	public BinaryNode ( int d){ 
		data = d;
	}
	
	public void addNode (int n) {
		if(n > data) {
			if ( right == null) {
				right = new BinaryNode( n);
				return;
			}
			else {
				right.addNode(n);
			}
		}
		if ( n < data) {
			if (left == null) {
				left = new BinaryNode(n);
				return;
			}
			else {
				left.addNode(n);
			}
		}
		
	} 
	public int getData() {
		return data;
	}
	public BinaryNode getRight () {
		return right;
	}
	public BinaryNode getLeft () {
		return left;
	}
	public void setLeft(BinaryNode node) {
		left = node;
		return;
	}
	public void setRight(BinaryNode node) {
		right = node;
		return;
	}

}
