import java.util.ArrayList;

public class DjikstraNode {
	private boolean visited;
	private int heldDistance;
	
	public DjikstraNode () {
		heldDistance = Integer.MAX_VALUE;
		this.visited = false;
	}
//	public boolean getVisited() {
//		return visited;
//	}
	public void setVisited ( Boolean A) {
		this.visited = A;
	}
	public boolean getVisited () {
		return this.visited;
	}
	public int getheldDistance () {
		return heldDistance;
	}
	public void setheldDistance (int A) {
		this.heldDistance = A;
	} 
	
}
