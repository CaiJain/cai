import java.util.ArrayList;

public class KnapSack_Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		NapSack_Items[] C = new NapSack_Items[5];
		NapSack_Items one = new NapSack_Items(3, 31);
		NapSack_Items two = new NapSack_Items(2, 24);
		NapSack_Items three = new NapSack_Items(8, 45);
		NapSack_Items four = new NapSack_Items(8, 49);
		NapSack_Items five = new NapSack_Items(1, 10);
		C[0] = one;
		C[1] = two;
		C[2] = three;
		C[3] = four;
		C[4] = five;
		
		NapSack_Problem A = new NapSack_Problem( 20 , C );
		System.out.println(A.sort());

	}

}
