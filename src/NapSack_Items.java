
public class NapSack_Items {
	public int weight;
	public int value;
	
	public NapSack_Items (int w , int v) {
		weight = w;
		value = v;
		
	}
	
	public int getWeight () {
		return weight;
	}
	public int getValue() {
		return value;
	}
	public void setWeight (int x) {
		weight = x;
	}
	public void setValue (int z) {
		value = z;
	}

}
