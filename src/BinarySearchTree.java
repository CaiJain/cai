import java.util.Scanner;
public class BinarySearchTree {
	public BinaryNode headNode;


	public BinarySearchTree (int value) {
		headNode =new BinaryNode(value);


	}
	public void printTree (BinaryNode node) {
		System.out.println(node.getData());
		if (node.right != null) {
			printTree(node.right);
		}
		if (node.left != null) {
			printTree(node.left);
		}


	}

	public boolean IsInTree (int d , BinaryNode node) {
		if ( d == node.getData()) {
			return true;
		}
		else if (d < node.getData()) {
			if (node.left == null) {
				return false;
			}
			else {
				return IsInTree(d, node.left);
			}

		}
		if ( node.right == null) {
			return false;
		}
		else if ( d > node.getData()) {
			return IsInTree(d, node.right);
		}

		return false;



	}

	public void add( int d, BinaryNode node) {
		if ( headNode == null) {
			headNode = new BinaryNode(d);
		} 
		if (IsInTree(d, node) == true) {
			return;		
		}
		else {
			headNode.addNode(d);
		}
	}
	public boolean isEmpty() {
		if (headNode == null) {
			return true;	
		}
		return false;
	}
	public BinaryNode getHeadNode() {
		return headNode;
	}
	public int getLength( BinaryNode node) { 
		int goLeft=0;
		int goRight=0;
		if ( node == null) {
			return 0;
		}
		if (node.left == null) {
			goLeft=0;
		}
		else {
			goLeft=getLength(node.getLeft());
		}
		if ( node.right == null) {
			goRight=0;
		}
		else {
			goRight=getLength(node.getRight());
		} 
		return goLeft + goRight + 1;
	}



	public void getSpot (int d, BinaryNode node) {
		int left = 0;
		int right = 0;
		if (IsInTree(d, node) == false) {
			System.out.println(" Not in tree");
			return;
		}
		if ( d == node.getData()) {
			System.out.println(" Done");
			return;
		}


		if ( d < node.getData()) {
			System.out.println(" 1 left");
			getSpot(d , node.left);
			return;
		}
		if ( d > node.getData()) {
			System.out.println(" 1 right");
			getSpot(d , node.right);
			return;
		}
		return;

	}
	public void remove ( int d , BinaryNode node) {
		if (d == headNode.getData()) {
			System.out.println("Can't remove head.");
			return;
		}
		if (node.left != null && d == node.left.getData()) {
			BinaryNode temp = node;
			BinaryNode tom = node.left.getLeft();
			BinaryNode tim = node.left.getRight();
			if (tim != null) {
				node.setLeft(tim);


				while (tim.left != null) {
					tim = tim.left;
				}
				tim.setLeft(tom);
				return;
			}
			else {
				temp.setLeft(tom);
				return;
			}

		}

		else if (node.right != null && d == node.right.getData()) {
			BinaryNode temp = node;
			BinaryNode tom = node.right.getLeft();
			BinaryNode tim = node.right.getRight();
			if (tim != null) {
				node.setRight(tim);


				while (tim.left != null) {
					tim = tim.left;
				}
				tim.setLeft(tom);
				return;
			}
			else {
				temp.setRight(tom);
				return;
			}

		}
		else if ( node.left ==null && node.right == null ) {
			return;
		}
		else if (d > node.getData()) {
			remove(d, node.right);
		}
		else 	if (d < node.getData()) {
			remove (d, node.left);
		}
	}
}






