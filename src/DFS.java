
public class DFS {
	
	
	public static int[] search (Adjacency_Matrix A) {
		int[] distances = new int[A.getNodeCount()];
		distances[0] = 0;
		for(int i = 1; i < A.getNodeCount() ; i ++) {
			distances[i] = Integer.MAX_VALUE;
		}
		Stacks <Integer>stack = new Stacks<Integer>();
		stack.push(0);
		while (stack.isEmpty() != true) {
			System.out.println(stack);
			int temp = stack.pop();
			for ( int i = 0 ; i < A.getNodeCount() ; i++) {
				if ( A.map[temp][i] == 1 && distances[i] > distances[temp] + 1 ) {
					distances[i] = distances[temp] + 1;
					stack.push(i);
				}
				
			}
		}
		return distances;
		
	}

}
