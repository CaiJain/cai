
public class Adjacency_Matrix {
	public int[][] map;
	private int nodeCount;
	
	public Adjacency_Matrix(int NodeCount) {
		this.map = new int[NodeCount][NodeCount];
		nodeCount = NodeCount;
		
		
	}
	public void fill (int a, int b) {
		map[a][b] = 1;
		map[b][a] = 1;
	}
	public boolean checkFill (int a , int b) {
		if (map[a][b] == 1) {
			return true;
		}
		return false;
	}
	public int getNodeCount () {
		return nodeCount;
		
	}
	public void fillWeighted (int a , int b, int weight) {
		map[a][b] = weight;
		map[b][a] = weight;
		
	}
	

}
