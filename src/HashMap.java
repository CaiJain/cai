import java.util.Arrays;

//HashMap<Integer, String> map = new HashMap

public class HashMap<A> {
	private Hnode<A>[] buckets;
	private int collisions;
	private int counter;
	public HashMap (){
		this.buckets = new Hnode[8];
		collisions = 0;

	}
	public int findBucket( String key) { 
		int out = 0;
		char[] charr = key.toCharArray();
		for ( int i = 0; i < charr.length ; i++) {
			int temp = (int)(charr[i]);
			out = out + temp ^ 40 * 34 / 59;
		}
		return out%buckets.length;
	}
	public void resize () {
		counter = 0;
		collisions = 0;
		Hnode<A>[] temp = Arrays.copyOf(buckets, buckets.length);
		buckets = new Hnode[temp.length*8];


		for ( int i = 0; i < temp.length ; i++) {
			if ( temp[i] != null) {
				Hnode<A> temps = temp[i];
				String a = temp[i].getKey();
				A b = temp[i].getdata();
				put(a, b);
				while( temps.getNext() != null) {
					temps = temps.getNext();
					a = temps.getKey();
					b = temps.getdata();
					put(a, b);
				}
			}


		}
	} 
	public void put( String key, A data) {
		Hnode<A> added = new Hnode<>(key, data);
		int bucket = findBucket(key);
		counter++;
		if ( counter > buckets.length/2) {
			resize();
		}
		Hnode<A> temp = buckets[bucket];
		if(temp == null) {
			buckets[bucket] = added;
		}
		else{
			while(temp.getNext() != null) {
				collisions++;
				System.out.println("We had a collision.");
				if (temp.getKey() == added.getKey()) {
					temp.setData(added.getdata());
					return;
				}
				temp = temp.getNext();
			}
			temp.setNext(added);
		}
	}
	public A get (String key){
		int bucket = findBucket(key);
		Hnode<A> temp = buckets[bucket];
		if (temp.getKey() == key) {
			return temp.getdata();
		}
		else {
			while (temp.getKey() != key) {
				if (temp.getNext() == null) {
					return null;
				}
				temp = temp.getNext();

			}
			return temp.getdata();

		}

	}
	public void remove (String key) {
		int bucket = findBucket(key);
		Hnode<A> temp = buckets[bucket];
		while(temp.getNext().getKey() != key) {
			if (temp.getNext() == null) {
				return;
			}
			temp = temp.getNext();
		}
		temp.setNext(temp.getNext().getNext());
	}
	public void getMap () {
		Hnode <A> temp = null;
		for ( int i = 0; i<buckets.length ; i++) {
			if (buckets[i] != null) {
				System.out.println(buckets[i].getdata());
				temp = buckets[i];
				while (temp.getNext() != null) {
					temp = buckets[i].getNext();
					System.out.println(temp.getdata());
			}

			}

		}


	}
}

