import java.util.ArrayList;
import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
public class File_io {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner P = new Scanner(System.in);
		dataBase a = new dataBase();
		File DBFile = new File("Cai");
		System.out.println("Do you want to connect to a server");
		if (P.next().equals("yes")) {
			System.out.println("Enter the IP address of the server you want to connect to");
			String IP = P.next();
			Client client = new Client(IP);
			client.printMessages();
			// doesn't interfere with main thread cause it is on another thread.
			while (true) {
				System.out.println("Enter a message that you want to send");
				client.sendMessage(P.next());
			}
			
		}
		else {
			Server server = new Server(a);
			server.start();
		}
		if (DBFile.exists()) {
			try {
				a = dataBase.ReadFromDisk("Cai");
			} catch (ClassNotFoundException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
	System.out.println("Do you want to delete all your messages?");
	if (P.next().equals("yes")) {
		a.removeMessages();
	}
	
		System.out.println("Do you want to see your old messages?");
		String L = P.next();
		if (L.equals("yes")) {
			
			for (int i = 0; i < a.messages.size() ; i++) {
		System.out.print(String.format( "%s," , a.messages.get(i) ));
			}
		}
		while (true) {
			System.out.println("Enter a message");
			String G = P.next();
			a.addMessage(G);
			try {
				a.WriteToDisk("Cai");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			}
		
			
			
			
			
			
		
		
		
		

	}
	
	public static class dataBase implements Serializable  {
		private static final long serialVersionUID = 1L;
		ArrayList<String> messages;
		
		public dataBase () {
			this.messages = new ArrayList<String>();
		}
		public void addMessage (String A) {
			this.messages.add(A);
			
		}
		public void WriteToDisk (String fileName) throws IOException {
			FileOutputStream out = new FileOutputStream(fileName);
			ObjectOutputStream Writer = new ObjectOutputStream(out);
			Writer.writeObject(this);

			
		}
		public static dataBase ReadFromDisk(String fileName) throws IOException, ClassNotFoundException {
			FileInputStream in  = new FileInputStream(fileName);
			ObjectInputStream reader = new ObjectInputStream(in);
			return (dataBase)reader.readObject();
			
			
		}
		public void removeMessages () {
			File A = new File("Cai");
			A.delete();
			
			
		}
		
	}
	public static class Server  {
		ServerSocket A;
		dataBase B;
		public Server (dataBase C ) {
			B = C;
			//database to write messages to.
			
			 try {
				 Scanner O = new Scanner(System.in);
				 System.out.println("What port do you want to listen on");
				A = new ServerSocket(O.nextInt());
				//connecting to port.
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		public void start () {
			//Make start method ask for messages and send them in different thread. Also add a print messages method. Works same way with client as server
			while (true) {
				try {
					Socket Connection = A.accept();
					new Thread(() -> {
						try {
							Scanner L = new Scanner(System.in);
							DataOutputStream out = new DataOutputStream(Connection.getOutputStream());
							while (true) {
							System.out.println("What do you want to send");
							String P = L.next();
							out.writeUTF(P);
							//UTF is incoding for Strings
							}
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}	
					}).start();
					
					
					//DataOutputStream out = new DataOutputStream(Connection.getOutputStream());
					//DataOutPut Stream over one we have in socket.
					DataInputStream in = new DataInputStream(Connection.getInputStream());
					//DataInputStream over one we have in socket.
					while (true) {
						String message = in.readUTF();
						B.addMessage(message);
						System.out.println(message);
						//UTF is way of encoding text.
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		
	}
	public static class Client {
		Socket B;
		public Client (String address) {
			try {
				Scanner l = new Scanner(System.in);
				System.out.println("What port do you want to connect to?");
				
				B = new Socket(address, l.nextInt());
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		public void sendMessage (String Message) {
			try {
				DataOutputStream Out = new DataOutputStream(B.getOutputStream());
				Out.writeUTF(Message);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		public void printMessages() {
			new Thread (() -> {
					try {
						DataInputStream in = new DataInputStream(B.getInputStream());
						while (true) {
							System.out.println(in.readUTF());
						}
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
				}
			).start();
			
		}
	}

}
