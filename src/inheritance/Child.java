package inheritance;

public class Child extends Parent {
	private int grade;
	public Child (int g , int a , String n) {
		super( n , a);
		 grade = g;
	}
	public int getGrade() {
		return grade;
	}
}
