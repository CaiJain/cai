package inheritance;

public class Parent {
	private String name;
	private int age;
	public Parent ( String n , int a) {
		name = n;
		age = a;
	}
	public int getAge () {
		return age;
	}
	public String getName () {
		return name;
	}
}
