package inheritance;

public class Node < T > {
	public T data;
	public Node next;
	public Node ( T data , Node next) {
		this.data=data;
		this.next=next;
	}
	public T getData () {
		return data;
	}
	public Node getNode () {
		return next;
	}
	public void setData ( T newdata) {
		this.data=newdata;
	}
	public void setNode ( Node newNode ) {
		this.next=newNode;
	}

	

}
