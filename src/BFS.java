import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
public class BFS {
	
	public void BFSing () {
		Adjacency_Matrix A = new Adjacency_Matrix(6);
		A.fill(0,1);
		A.fill(0, 2);
		A.fill(1, 4);
		A.fill(2,3);
		A.fill(3, 4);
		A.fill(4, 5);
		int[] distances = new int[6];
		distances[0] = 0;
		for(int i = 1; i < 6 ; i ++) {
			distances[i] = Integer.MAX_VALUE;
		}
		Queue <Integer>queue = new Queue<Integer>();
		queue.add(0);
		while (queue.isEmpty() != true) {
			int temp = queue.remove();
			for ( int i = 0 ; i <6 ; i++) {
				if ( A.map[temp][i] == 1 && distances[i] == Integer.MAX_VALUE) {
					distances[i] = distances[temp] + 1;
					queue.add(i);
				}
				
			}
		}
		System.out.println(Arrays.toString(distances));
		
		
	}
	public static int[] search (Adjacency_Matrix A) {
		int[] distances = new int[A.getNodeCount()];
		distances[0] = 0;
		for(int i = 1; i < A.getNodeCount() ; i ++) {
			distances[i] = Integer.MAX_VALUE;
		}
		Queue <Integer>queue = new Queue<Integer>();
		queue.add(0);
		while (queue.isEmpty() != true) {
			int temp = queue.remove();
			for ( int i = 0 ; i <A.getNodeCount() ; i++) {
				if ( A.map[temp][i] == 1 && distances[i] == Integer.MAX_VALUE) {
					distances[i] = distances[temp] + 1;
					queue.add(i);
				}
				
			}
		}
		return distances;
		
	}
	public static void main (String[] args) {
//		BFS B = new BFS();
//		B.BFSing();
//		Adjacency_Matrix C = new Adjacency_Matrix(103); 
//		Random A = new Random();
//		
//		
//		for(int i = 0; i < 200 ; i++) {
//			int z =A.nextInt(103);
//			int b = A.nextInt(103);
//			C.fill(z, b);
//		}
//		System.out.println(Arrays.toString(BFS.search(C)));
//		
		
		Adjacency_Matrix example = new Adjacency_Matrix(5);
		example.fill(0,1);
		example.fill(0, 2);
		example.fill(1, 3);
		example.fill(2,4);
		example.fill(3, 4);
		System.out.println(Arrays.toString(DFS.search(example)));
		
		
		
	}

}
