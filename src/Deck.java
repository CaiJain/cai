import java.util.Random;
public class Deck {
	private Card[] cards;
	private int size;
	public Deck() {
		size = 52;
		cards = new Card[52];
		int count = 0;
		for (int l = 1; l<14 ; l++) {
			Card A = new Card(l , "Hearts");
			cards[count] = A;
			count++;
		}
		for (int l = 1; l<14 ; l++) {
			Card A = new Card(l , "Spades");
			cards[count] = A;
			count++;
		}
		for (int l = 1; l<14 ; l++) {
			Card A = new Card(l , "Clubs");
			cards[count] = A;
			count++;
		}
		for (int l = 1; l<14 ; l++) {
			Card A = new Card(l , "Diamonds");
			cards[count] = A;
			count++;
		}
		
		
	}
	public void Shuffle() {
		Random R = new Random();
		for ( int i = 0; i < 52 ; i++) {
			int r = R.nextInt(52);
			Card temp = cards[i];
			cards[i] = cards[r];
			cards[r] = temp;
			
		}
	}
	public Card deal() {
		size --;
		return cards[size];
	}
	
	}
