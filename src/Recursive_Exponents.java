import java.util.Scanner;
public class Recursive_Exponents {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner A = new Scanner(System.in);
		System.out.println("What number would you like to add an exponent to");
		int q = A.nextInt();
		System.out.println("What exponent would you like");
		int w = A.nextInt();
		System.out.println(exponents(q , w));
	}
	public static int exponents (int y , int x ) {
		if ( x== 0) {
			return 1;
		}
		return y * exponents( y, x-1 );
		// y * ( 
		// 2 * (
		// exponents(2, 0) == 1
		// exponents(2, 1) == 2 * 1
	}

}
