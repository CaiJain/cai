import java.util.Scanner;
public class Towers_of_Hanoi {

	public static void main(String[] args) {
		Scanner A = new Scanner(System.in);
		System.out.println(" Type the number of disks you would like");
		int x = A.nextInt();
		if ( x%2 == 0) {
			Hanoieven(x , "A" , "B", "C");
		}
		else {
			Hanoiodd(x, "A" , "B", "C");
		}
	}
	public static void Hanoiodd( int amount, String a , String b , String c) {
		if (amount <= 1 ) {
			System.out.println("Move " + a + " to " + b);
			return;
	}
		Hanoieven (amount - 1 , a , c , b);
		System.out.println( "Move " + a + " to " + b);
		Hanoiodd (amount - 1 , c , b , a);

	}
	
	public static void Hanoieven( int amount, String a , String b , String c) {
		if (amount <= 1 ) {
			System.out.println("Move " + a + " to " + b);
			return;
	}
		Hanoieven (amount - 1 , a , c , b);
		System.out.println( "Move " + a + " to " + b);
		Hanoieven (amount - 1 , c , b , a);

	}
}
