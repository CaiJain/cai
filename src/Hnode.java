
public class Hnode<A> {
	private String key;
	private A data; 
	private Hnode<A> next;
	
	public Hnode ( String key, A data) {
		this.key = key;
		this.data = data;
	}
	public Hnode<A> getNext() {
		return next;
	}
	public String getKey () {
		return key;
	}
	public A getdata () {
		return data;
	}
	public void setNext (Hnode<A> A) {
		this.next = A;
	}
	public void setData (A hello){
		this.data = hello;
	}
}
