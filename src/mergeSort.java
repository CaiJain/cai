import java.util.Arrays;
import java.util.Random;
public class mergeSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] A = new int [10];
		Random rand = new Random();
		for(int i = 0 ; i < 10 ; i++) {
			A[i] = rand.nextInt(10) + 1;
			System.out.print(A[i]+" ");
		} 
		System.out.println( "\n");
		//int[] A = {3, 2, 4, 1, 5, 6, 7};
		A = mergesort(A);
		for(int i = 0 ; i < A.length ; i++) {
			System.out.print(A[i] + " ");
		} 
	}
	public static int[] mergesort(int[] list) {
		if(list.length == 1 || list.length == 0) {
			return list;
		}

		int temp = list.length/2;

		int[] listsmall = new int[temp];
		int[] listbig = new int[list.length-temp];

		for(int i = 0; i < temp ; i++) {
			listsmall[i] = list[i];
		}
		for ( int i = 0; i <list.length-temp ; i++) {
			listbig[i] = list[temp + i];
		}

		//System.out.println(Arrays.toString(listsmall)+" "+Arrays.toString(listbig));
			listsmall = mergesort(listsmall);
		listbig = mergesort(listbig);
	

		int l = 0;
		int j = 0;
		int[] Y = new int[list.length];
		while ( l < listsmall.length && j < listbig.length) {
			if ( listsmall[l] < listbig[j] ) {
				Y[l + j] = listsmall[l];
				l++;
			}
			else {
				Y[l + j] = listbig[j];
				j++;
			}
		}

		if ( l == listsmall.length) {
			while (j < listbig.length ) {
				Y[l + j] = listbig[j];
				j++;
			}
		}
		else if(j == listbig.length) {
			while (l < listsmall.length ) {
				Y[l + j] = listsmall[l];
				l++;
			}
			
		}
		else {
			System.out.println("Cai failed");
		}
		return Y;
	}
}

