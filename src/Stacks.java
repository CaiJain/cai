import java.util.ArrayList;
public class Stacks<T> {
	private ArrayList<T> Stack;
	public int length;
	public Stacks () {
		length = 0;
		Stack = new ArrayList<T>();
	}
	public void push ( T p) {
		Stack.add(p);
		length++;
	}
	public T pop () {
		T temp = Stack.remove(length - 1);
		length--;
		return temp;
	}
	public int getlength() {
		return length;
	}
	public T getTop() {
		return Stack.get(length - 1);
	}
	public void checkEmpty() {
		if (length ==0) {
			System.out.println(" The list is empty.");
		}
	}
	public boolean isEmpty () {
		return length == 0;
	}
	public String toString() {
		return Stack.toString();
	}
	

}
