import java.awt.Window.Type;
import java.util.ArrayList;
import java.util.Vector;

public class DAL {
    public ArrayList<Vector>[] map;
    private int nodeCount;

    public DAL (int NodeCount) {
        this.map = new ArrayList[NodeCount];
        for (int i = 0; i < NodeCount ; i++) {
            map[i] = new ArrayList();
        }
        nodeCount = NodeCount;
    }
    public int getLength() {
        return map.length;
    }
    public void fill (Vector<Integer> b, int a) {
        map[a].add(b);


    }
    public boolean checkFill (int a ) {
        if (map[a].size() > 0 ) {
            return true;

        }
        return false;
    }
    public int getNodeCount () {
        return nodeCount;
    }
    public void remove (Vector<Integer> a, int b) {
        for (int i = 0 ; i < map[b].size()  ; i++ ) {
            if (map[b].get(i) == a ) {
                map[b].remove(i);
        }
        }
    }
    public boolean checkVisited (DjikstraNode[] A) {
        for ( int i = 0; i < A.length  ; i++) {
            if (A[i].getVisited() == false) {
                return false;
            }

        }

        return true;
}
    

    

public void printMap () {
    for ( int i = 0; i < map.length ; i ++) {
        System.out.println(map[i]);
    }
}


public int Djikstra_Algorithm (int place, DAL B) {
    DjikstraNode[] A = new DjikstraNode[B.getLength()];
    for (int i = 0; i < A.length ; i++) {
        A[i] = new DjikstraNode();
        System.out.println(A[i].getVisited() + " Is the visited");
    }
    A[0].setheldDistance(0);
    int i = 0;
    int l = 0;
    while (checkVisited(A) != true) {
        System.out.println(A[i].getheldDistance() + " is the held distance of " + i);
        A[i].setVisited(true);
        System.out.println(A[i].getVisited() +" Is the visited of" + i);
        for ( int f = 0; f < map[i].size() ; f++) {

           int x = (int)(((Vector)(B.map[i].get(f))).get(0));
           System.out.println(x + " Is the value of x");
           if (A[x].getVisited() == true) {
               System.out.println(" In the if loop");

           }
           else if (A[x].getheldDistance() == Integer.MAX_VALUE || A[x].getheldDistance() > (int)(((Vector)(B.map[i].get(f))).get(1))) {
               System.out.println(" In this else if");
            A[x].setheldDistance( (int)(((Vector)(B.map[i].get(f))).get(1)) + A[i].getheldDistance()); 
            System.out.println(A[x].getheldDistance()+ "Is the held distance of spot" + x);
           }


        }
        i++;
        
    }
    return A[place].getheldDistance();

   
}


public static void main(String[] args) {
    DAL A= new DAL(5);
    Vector V = new Vector();
    V.add(1);
    V.add(3);
    Vector F = new Vector();
    F.add(2);
    F.add(1);
    Vector G = new Vector();
    G.add(4);
    G.add(3);
    Vector C = new Vector();
    C.add(3);
    C.add(5);
    Vector H = new Vector();
    H.add(3);
    H.add(10);
    Vector I = new Vector();
    I.add(3);
    I.add(15);
    A.fill(V,0);
    A.fill(F,0);
    A.fill(G,0);
    A.fill(C, 1);
    A.fill(H, 2);
    A.fill(I, 4);
    Vector test = new Vector();
    System.out.println((int)(((Vector)(A.map[0].get(0))).get(1)));
    System.out.println(A.checkFill(0));
    A.printMap();
    System.out.println(A.Djikstra_Algorithm(3,A));
}
}




