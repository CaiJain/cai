import java.util.ArrayList;
import java.util.Random;

public class QuickSort {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] A = new int [10];
		Random rand = new Random();
		for(int i = 0 ; i < 10 ; i++) {

			A[i] = rand.nextInt(10) + 1;
			System.out.println(A[i]);
		} 
		A=quicksort(A);
		System.out.println();
		for ( int i = 0 ; i < 10 ; i ++) {
		System.out.println(A[i]);
		}

	}
	public static int[] quicksort(int[] list) {
		if (list.length == 1 || list.length == 0) {
			return list;
		}
		int pivot = list[list.length - 1];
		int pposition = list.length - 1;
		for ( int i = 0 ; i < pposition ; i++) {
			if ( pivot < list[i]) {
				list[pposition] = list[i] ;
				int temp = list[pposition - 1];
				list[pposition - 1] = pivot;
				list[i] = temp;
				pposition--;
				i--;
			}

		} 
		int[] listsmall= new int[pposition];
		int[] listbig = new int[list.length - 1 - pposition];
		for(int i = 0 ; i < listsmall.length; i ++){
			listsmall[i] = list[i];
		}
		for(int i = 0 ; i < listbig.length ; i++ ) {
			listbig[i] = list[i + 1 + pposition];
		}
		listsmall = quicksort(listsmall);
		listbig = quicksort(listbig);
		for ( int i = 0 ; i < listsmall.length; i++) {
			list[i] = listsmall[i];
		}
		list[pposition] = pivot;
		for ( int i = pposition + 1 ; i < list.length ; i++) {
			list[i] = listbig[i - pposition - 1];
			
		}
		return list;
	}




}


