import java.util.ArrayList;

public class Adjacency_List {
    public ArrayList<Integer>[] map;
    private int nodeCount;

    public Adjacency_List (int NodeCount) {
        this.map = new ArrayList[NodeCount];
        for (int i = 0; i < NodeCount ; i++) {
            map[i] = new ArrayList();
        }
        nodeCount = NodeCount;
    }
    public void fill (int a , int b) {
        map[a].add(b);
        map[b].add(a);
    }
    public boolean checkFill (int a , int b) {
        if (map[a].size() > 0 && map[b].size() > 0) {
            return true;

        }
        return false;
    }
    public int getNodeCount () {
        return nodeCount;
    }
    public void remove (int a , int b) {
        for ( int i = 0 ; i < map[a].size(); i++) {
            if (map[a].get(i) == b ) {
                map[a].remove(i);
        }
       
    }
    for (int i = 0 ; i < map[b].size()  ; i++ ) {
        if (map[b].get(i) == a ) {
            map[b].remove(i);
    }
    }
    
}
public void printMap () {
    for ( int i = 0; i < map.length ; i ++) {
        System.out.println(map[i]);
    }
}

public static void main(String[] args) {
    Adjacency_List A = new Adjacency_List(3);
    A.fill(1,0);
    A.fill(2, 1);
    System.out.println(A.checkFill(0, 2));
    A.printMap();
}
}