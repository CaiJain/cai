package Shapes;

public class Triangle extends Shape {
	private int base;
	private int height;
	private int Rline;
	private int Lline;
	public Triangle ( int l , int r , int b , int h , String c , String n) {
		super( 3, c , n);
		Rline = r;
		base = b;
		height = h;
	}
	public int getPerimeter() {
		return Rline + Lline + base;
	}
	public int getArea() {
		return base * height /2;
	}

}
