package Shapes;

public abstract class Shape implements Shapes {
	private int sides;
	private String color;
	private String name;
	
	public Shape (int s , String c , String n) {
		sides = s;
		color = c;
		name = n;
		
	}
	public int getSides() {
		return sides;
	}
	public String getColor() {
		return color;
	}
	public String getName() {
		return name;
	}
	public abstract int getPerimeter();
	public abstract int getArea();
		
	

}
