package Shapes;

public class Rectangle extends Shape {
	 private int length;
	 private int width;
	 public Rectangle( int l , int w ,String c , String n) {
		 super ( 4 , c , n);
		 length = l;
		 width = w;
	 }
	 public int getPerimeter() {
		 return length * 2  + width * 2;
	 }
	 public int getArea() {
		 return length * width;
	 }
	 
	 

}
