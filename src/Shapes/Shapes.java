package Shapes;

public interface Shapes {
	public int getSides();
	public String getColor();
	public String getName();
	public int getPerimeter();
	public int getArea();

}
