package Shapes;

public class Trapezoid extends Shape {
	private int topBase;
	private int bottomBase;
	private int height;
	private int rightSide;
	private int leftSide;
	public Trapezoid(String c , String n , int t, int b , int h , int l , int r) {
		super(4 , c , n );
		topBase = t;
		bottomBase = b;
		height = h;
		rightSide = r;
		leftSide = l;
	}
	public int getArea() {
		return topBase * bottomBase/2 * height;
	}
	public int getPerimeter() {
		return topBase + bottomBase + rightSide + leftSide;
	}
	

}
