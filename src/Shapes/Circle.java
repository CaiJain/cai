package Shapes;

public class Circle extends Shape {
	private int radius;
	public Circle( int s , String c , String n , int r){
		super(   0 , c , n);
	radius = r;
	}
	public int getArea() {
		return (int)(Math.PI * Math.pow(radius, 2));
	}
	public int getPerimeter() {
		return (int)(Math.PI * radius * 2);
	}

}
