import java.util.ArrayList;

public class Player {
	private ArrayList<Card> hand;
	private String name;
	
	public Player(String n) {
		hand = new ArrayList<Card>();
		name = n;
	}
	public String getName() {
		return name;
	}
	public void addCard( Card A) {
		hand.add(A);
		if (A.getValue() == 11 || A.getValue() == 12 || A.getValue() == 13) {
			Card temp = new Card(10 , "jacks");
			hand.remove(hand.size() - 1);
			hand.add(temp);
		}
		
	}
	public void getHand() {
		System.out.println(hand);
	}
	public int getHandValue() {
		int count = 0;
		for ( Card c: hand) {
			count += c.getValue();
		}
		return count;
	}
	public void clear() {
		hand.clear();
	}

}
