public class MATH {
    public static void main (String[] args) {
        for ( int i = 0 ; i < 11 ; i++) {
            if(i < 5){
                System.out.println(i);
            }
            else{            
                System.out.println(i+2);
            }
        }
        for ( int i = 0 ; i < 11 ; i++) {
            System.out.println(i);

        }
    }

void sort(int arr[]) 
    { 
        int n = arr.length; 
  
        // One by one move boundary of unsorted subarray 
        for (int i = 0; i < n-1; i++) 
        { 
            // Find the minimum element in unsorted array 
            int min_idx = i; 
            for (int j = i+1; j < n; j++) 
                if (arr[j] < arr[min_idx]) 
                    min_idx = j; 
  
            // Swap the found minimum element with the first 
            // element 
            int temp = arr[min_idx]; 
            arr[min_idx] = arr[i]; 
            arr[i] = temp; 
        } 
    } 

  
public static int[]heapSort (int[] A) {
    //ArrayList<Integer> B = new ArrayList<Integer>();
    int n = 0;

    for ( int i = A.length - 1; i >= 0 ; i--) {
        n = i;
        int a = n%2;
        while (n != 0 && ((A[n] < A[(n-2) / 2] && a== 0) || (A[n] < A[(n-1) / 2] && a==1))) {
            if ( a== 0) {
                int temp = A[(n-2)/2];
                A[(n-2)/2] = A[n];
                A[n] = temp;
                n = (n-2)/2;
                a = n%2;
                i = A.length;

            }
            else if ( a== 1) {
                int temp = A[(n-1)/2];
                A[(n-1)/2] = A[n];
                A[n] = temp;
                n = (n-1)/2;
                a = n%2;
                i = A.length ;

            }

        }
    }
    
    
    for ( int i = 0 ; i < A.length ; i++ ) {
        
        int temp = A[A.length - i - 1];
        A[A.length - i - 1] = A[0];
        A[0] = temp;
        n = 0;
        
        while (( n*2 + 2 < A.length - i -1 && A[n*2 + 2] < A[n] )||( n*2 + 1 < A.length - i - 1 &&  A[n*2+1] < A[n])) {
            if ( n*2+2 < A.length - i - 1 && A[n*2 + 2] < A[n*2 + 1] ) {
                int temps = A[n*2 + 2];
                A[n*2 + 2] = A[n];
                A[n] = temps;
                n = n*2 + 2;
            }
            else {
                int temps = A[n*2 + 1];
                A[n*2+1] = A[n];
                A[n] = temps;
                n = n*2 + 1;


            }
        }
    }
    
    for (int i = 0 ; i < A.length/2 ; i ++) {
        int temp = A[A.length - i - 1];
        A[A.length - i -1] = A[i];
        A[i]= temp;
    }
    return A;
}
}
    