import java.util.ArrayList;

public class MaxHeap {
	private ArrayList<Integer> A;	
	public MaxHeap () {
		A =new ArrayList<Integer>();

	}
	public void add (int value) {
		A.add(value);
		int n = A.size() - 1;
		while ( (A.get(n) > A.get((n-1)/2) && n%2 == 1)|| (A.get(n) > A.get((n-2)/2) && n%2 == 0 ))  {
			int temp = 0;
			if ( n%2 == 1) {
				temp = A.get((n-1)/2);
				A.set(((n-1)/2), A.get(n));
				A.set(n, temp);
				n = (n-1)/2;
			}
			else if (n%2 == 0) { 
				temp = A.get((n-2)/2);
				A.set(((n-2)/2), A.get(n));
				A.set(n,  temp);
				n = (n-2)/2;
			}
		}

	}
	public int getMin () {
		return A.get(0);        

	}
	public int removeHead () {
		int head = A.get(0);
		A.set(0, A.get(A.size() - 1));
		A.remove(A.size() - 1);
		int n = 0;
		// 2n + 1 left child
		//2n + 2 right child
		while ( (A.get(n) < A.get(2 *n+1)  && (n*2 + 1 < A.size()))|| (A.get(n) < A.get(2*n+ 2) && (n*2 + 2 < A.size())))   {
			if (2*n + 2 > A.size()|| A.get(n*2 + 2) < A.get(2*n+1)) {
				int temp = A.get(n);
				A.set(A.get(n),A.get(2*n + 1));
				A.set(A.get(2*n + 1), temp);
				n = n*2 + 1; 
			}
			else if (2*n + 1 < A.size()&& A.get(n*2 + 1) > A.get(2*n+2)) {
				int temp = A.get(n);
				A.set(A.get(n),A.get(2*n + 2));
				A.set(A.get(2*n + 2), temp);
				n = n*2 + 2; 
			}
		}
		return head;

	}
	public void remove (int i) {
		A.set(i, Integer.MIN_VALUE);
		int n = i;

		while(n > 0) {
			if ( n%2 == 0) {
				int temp = (n-2)/2;
				A.set(temp, A.get(n));
				A.set(i, temp);
				n = n/2 - 2;
			}
			else if ( n%2 == 1) {
				int temp = (n-1)/2;
				A.set(temp, A.get(n));
				A.set(i, temp);
				n = n/2 - 1;
			}

		}
		removeHead();


	}
	public void changeValue (int n, int value ) {
		int a = n%2;
		A.set(n, value);

		if ((A.get(n*2 + 2) > A.get(n) )|| (A.get(n*2+1) > A.get(n) && n*2 + 1 < A.size() - 1)) {
			while (A.get(n*2 + 2) < A.get(n) || A.get(n*2+1) < A.get(n) ) {
				if (A.get(n) > A.get(n*2 + 1)) {
					int temp = A.get(n*2 + 1);
					A.set(n*2 + 1, A.get(n));
					A.set(n, temp);
					n = n*2 + 1;
				}
				else {
					int temp = A.get(n*2 + 2);
					A.set(n*2 + 2, A.get(n));
					A.set(n, temp);
					n = n*2 + 2;

				}
			}
		}
		else if (( A.get((n-2)/2) > A.get(n) && a==0) ||  (A.get((n-1)/2) > A.get(n) && a==1)  ) {
			while ( A.get((n-2)/2) > A.get(n) || A.get((n-1)/2) > A.get(n) ){
				if ( a== 0) {
					int temp = A.get((n-2)/2);
					A.set((n-2)/2, A.get(n));
					A.set(n, temp);
					n = (n-2)/2;
					a = n%2;
				}
				else if ( a==1) { 
					int temp = A.get((n-1)/2);
					A.set((n-1)/2, A.get(n));
					A.set(n, temp);
					n = (n-1)/2;
					a = n%2;

				}
			}

		}

	}

}
